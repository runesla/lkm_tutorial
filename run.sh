#!/bin/bash

set -e

RPI_CMD="host"
DATE=$(date -u +%Y%m%d-%H%M%S)
SRC_DIR="$(realpath $(dirname $0))"
RSYNC_ARGS="-rt --exclude=logs/ --exclude=.*"
BUILD_DIR="lkm_tutorial"

trap quit INT

function quit()
{
    echo "Cleaning up"
    ssh $RPI "pkill -u $(whoami)" &> /dev/null || true
    echo "Log files: "
    ls -lh logs/$DATE-*.log
}

mkdir -p logs

while [ $# -gt 0 ] ; do
    arg=$1
    shift

    case $arg in
        --clean)
            CLEAN="clean"
            ;;
        --args)
            RPI=$1
            shift
            ;;
    esac
done

echo "Using $RPI"

echo "Synching source"

rsync ${RSYNC_ARGS} ${SRC_DIR}/ $RPI:${BUILD_DIR}/

echo "Compiling on $RPI"

ssh -t $RPI "cd $BUILD_DIR/build && make ${CLEAN}" || exit $?

echo "Running on $RPI"

stdbuf -oL -eL ssh $RPI "cd $BUILD_DIR/build && stdbuf -oL -eL ./$RPI_CMD" |& tee logs/$DATE-rpi.log &

wait

echo "Done!"

quit
